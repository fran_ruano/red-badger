# README #

After reading and understanding the initial task, I can see it is not a problem adapted for a mobile tech task as it does not allow me to use many of the modern Swift features as 
protocols, functional programming, etc. Also, the UI it is almost not necessary.

My approach to solve the issue in an efficient way and showing to certain degree my skills was to create the models and the logic and test it. That way, it could see that I understand 
some concepts.

In order to run the tests CMD + U
If we want to add of morify the tests:


- ValidInput.txt includes a text example copied from the exercise definition
