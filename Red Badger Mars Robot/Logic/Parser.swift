//
//  Created by Fran Ruano on 20/4/21.
//

import Foundation

class Parser {
    
    func getPlanetGrid(_ input: String) -> PlanetGrid? {
        
        let inputBreakLine = input.split(separator: "\n").map { String($0) }
        guard inputBreakLine.count >= 3 else { return nil }
        
        let gridSizeLine = inputBreakLine.first?.split(separator: " ").map { String($0) }
        let gridSizeParsed = parseCoordinates(gridSizeLine)
        
        guard let gridSize = gridSizeParsed else { return nil }
        
        var robotArray = [RobotStart]()
        for index in stride(from: 1, to: inputBreakLine.count, by: 2){
            let initLine = inputBreakLine[index].split(separator: " ").map { String($0) }
            let movementParsed = parseMovement(inputBreakLine[index + 1])
            
            if let positionParsed = parsePosition(initLine) {
                
                let robot = RobotStart(position: positionParsed, movement: movementParsed)
                robotArray.append(robot)
            }

        }
        
        return PlanetGrid(gridSize: gridSize,
                          robots: robotArray)
    }
    
    private func parseMovement(_ item: String) -> [Movement] {
        
        var movement = [Movement]()
        for char in item {
            if let element = Movement(rawValue: char) {
                movement.append(element)
            }
        }
        
        return movement
    }
    
    private func parsePosition(_ item: [String]?) -> Position? {
        guard let validItem = item, validItem.count >= 3,
              let coordinatesParsed = parseCoordinates(validItem),
              let directionParsed = parseDirection(validItem) else { return nil }
        
        return Position(coordinates: coordinatesParsed,
                        direction: directionParsed)
    }
    
    private func parseDirection(_ item: [String]) -> Direction? {
        guard let element = item.last else { return nil }
        
        return Direction(rawValue: element)
    }
    
    private func parseCoordinates(_ item: [String]?) -> (x: Int, y: Int)? {
        guard let validItem = item, validItem.count >= 2,
              let xItem = Int(validItem[0]), let yItem = Int(validItem[1]) else { return nil }
        
        return (x: xItem, y: yItem)
    }
}
