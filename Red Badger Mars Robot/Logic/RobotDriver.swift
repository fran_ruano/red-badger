//
//  Created by Fran Ruano on 19/4/21.
//

import Foundation

class RobotDriver {
    
    let planetGrid: PlanetGrid
    private(set) var robotsDestiny: [RobotEnd]
    private(set) lazy var ignoredMovement = [(position: Position, movement: Movement)]()
    
    init(_ planetGrid: PlanetGrid) {
        
        self.planetGrid = planetGrid
        robotsDestiny = [RobotEnd]()
    }

    func moveRobotsToDestiny() {
        
        var robotsDestiny = [RobotEnd]()
        for robot in planetGrid.robots {
            var position = robot.position
            var status = Status.active
            
            for movement in robot.movement {

                if shouldIgnoreMovement(movement, position: position) {
                    continue
                }
                let newPosition = updatePosition(movement,
                                                 position: position)
                
                status = validateCoordinate(newPosition.coordinates)

                if status == .loss {
                    
                    ignoredMovement.append((position: position, movement: movement))
                    break
                }
                position = newPosition
            }
            robotsDestiny.append(RobotEnd(position: position,
                                          status: status))
        }
        
        self.robotsDestiny = robotsDestiny
    }
    
    private func updatePosition(_ movement: Movement,
                                position: Position) -> Position {
        var coordinates = position.coordinates
        var direction = position.direction
        switch movement {
        case .left:
            direction = moveToLeft(direction)
        case .right:
            direction = moveToRight(direction)
        case .forward:
            coordinates = moveForward(position)
        }
        
        return Position(coordinates: coordinates,
                        direction: direction)
    }
    
    private func moveToLeft(_ direction: Direction) -> Direction {
        
        switch direction {
        case .north:
            return .west
        case .south:
            return .east
        case .east:
            return .north
        case .west:
            return .south
        }
    }
    
    private func moveToRight(_ direction: Direction) -> Direction {
        
        switch direction {
        case .north:
            return .east
        case .south:
            return .west
        case .east:
            return .south
        case .west:
            return .north
        }
    }
    
    private func moveForward(_ position: Position) -> (x: Int, y: Int) {
        
        var coordinates = position.coordinates
        switch position.direction {
        case .north:
            coordinates.y += 1
        case .south:
            coordinates.y -= 1
        case .east:
            coordinates.x += 1
        case .west:
            coordinates.x -= 1
        }
        
        return coordinates
    }
    
    private func validateCoordinate(_ coordinates: (x: Int, y: Int)) -> Status {
        if coordinates.x < 0 ||
            coordinates.x > planetGrid.gridSize.x ||
            coordinates.y < 0 ||
            coordinates.y > planetGrid.gridSize.y {
            
            return .loss
        }

        return .active
    }
    
    private func shouldIgnoreMovement(_ movement: Movement, position: Position) -> Bool {
        
        let shouldIgnore = ignoredMovement.filter({ $0.position.coordinates.x == position.coordinates.x &&
            $0.position.coordinates.y == position.coordinates.y &&
            $0.position.direction == position.direction &&
            $0.movement == movement
        }).count
        
        return shouldIgnore > 0
    }
}
