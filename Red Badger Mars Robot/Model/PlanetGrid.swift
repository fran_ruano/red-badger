//
//  Created by Fran Ruano on 19/4/21.
//

import Foundation

enum Direction: String {
    case north = "N"
    case south = "S"
    case east = "E"
    case west = "W"
    
}

enum Movement: Character {
    case left = "L"
    case right = "R"
    case forward = "F"
}

struct RobotStart {
    
    let position: Position
    let movement: [Movement]
}

struct Position {
    
    let coordinates: (x: Int, y: Int)
    let direction: Direction
}
 
struct PlanetGrid {
    
    let gridSize: (x: Int, y: Int)
    let robots: [RobotStart]
}
