//
//  Created by Fran Ruano on 19/4/21.
//

import Foundation

enum Status {
    case loss
    case active
}

struct RobotEnd {
    
    let position: Position
    let status: Status
    
    var description : String {
        
        var value = "\(position.coordinates.x) \(position.coordinates.y) \(position.direction.rawValue)"
        value += (status == .loss) ? " LOST" : ""
        
        return value
    }
}
