//
//  Created by Fran Ruano on 20/4/21.
//

import Foundation

struct Helper {
    
    static func loadValidFile() -> String? {
        
        let bundle = Bundle(for: Swift.type(of: self))
        if let path = bundle.path(forResource: "ValidContent",
                                  ofType: "txt") {
            
            return try? String(contentsOfFile: path,
                               encoding: String.Encoding.utf8)
        }
        
        return nil
    }
}
