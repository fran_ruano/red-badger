//
//  Created by Fran Ruano on 20/4/21.
//

import XCTest
@testable import Red_Badger_Mars_Robot

class ParserTests: XCTestCase {

    var parser: Parser!
    
    override func setUpWithError() throws {
        
        parser = Parser()
    }

    override func tearDownWithError() throws {
        
        parser = nil
    }
    
    func testGetPlanerGridWhenDataIsInvalid() throws {
        
        let planetGrid = parser.getPlanetGrid("")
        XCTAssertNil(planetGrid)
    }

    func testGetPlanerGridWhenDataIsValid() throws {
        
        let content = Helper.loadValidFile()
        let planetGrid = parser.getPlanetGrid(content!)
        
        XCTAssertEqual(planetGrid?.gridSize.x, 5)
        XCTAssertEqual(planetGrid?.gridSize.y, 3)
        XCTAssertEqual(planetGrid?.robots.count, 3)
        
        XCTAssertEqual(planetGrid?.robots[0].position.coordinates.x, 1)
        XCTAssertEqual(planetGrid?.robots[0].position.coordinates.y, 1)
        XCTAssertEqual(planetGrid?.robots[0].position.direction, Direction.east)
        XCTAssertEqual(planetGrid?.robots[0].movement,
                       [.right, .forward, .right, .forward, .right, .forward, .right, .forward])
        
        XCTAssertEqual(planetGrid?.robots[1].position.coordinates.x, 3)
        XCTAssertEqual(planetGrid?.robots[1].position.coordinates.y, 2)
        XCTAssertEqual(planetGrid?.robots[1].position.direction, Direction.north)
        XCTAssertEqual(planetGrid?.robots[1].movement,
                       [.forward, .right, .right, .forward, .left, .left, .forward, .forward, .right, .right, .forward, .left, .left])

        XCTAssertEqual(planetGrid?.robots[2].position.coordinates.x, 0)
        XCTAssertEqual(planetGrid?.robots[2].position.coordinates.y, 3)
        XCTAssertEqual(planetGrid?.robots[2].position.direction, Direction.west)
        XCTAssertEqual(planetGrid?.robots[2].movement,
                       [.left, .left, .forward, .forward, .forward, .left, .forward, .left, .forward, .left])

    }

}
