//
//  Created by Fran Ruano on 19/4/21.
//

import XCTest
@testable import Red_Badger_Mars_Robot

class RobotDriverTests: XCTestCase {

    var robotDriver: RobotDriver!
    var planetGrid: PlanetGrid!
    
    override func setUpWithError() throws {
        
        planetGrid = PlanetGrid(gridSize: (x: 5, y: 3),
                                robots: [RobotStart(position: Position(coordinates: (x: 1, y: 1),
                                                                       direction: Direction.east),
                                                    movement: [.right, .forward, .right, .forward, .right, .forward, .right, .forward]),
                                         RobotStart(position: Position(coordinates: (x: 3, y: 2),
                                                                       direction: Direction.north),
                                                    movement: [.forward, .right, .right, .forward, .left, .left, .forward, .forward, .right, .right, .forward, .left, .left]),
                                         RobotStart(position: Position(coordinates: (x: 0, y: 3),
                                                                       direction: Direction.west),
                                                    movement: [.left, .left, .forward, .forward, .forward, .left, .forward, .left, .forward, .left])])
    }

    override func tearDownWithError() throws {
        
        robotDriver = nil
        planetGrid = nil
    }

    func testInit() throws {
        
        robotDriver = RobotDriver(planetGrid)
        
        XCTAssertEqual(robotDriver.planetGrid.gridSize.x, 5)
        XCTAssertEqual(robotDriver.planetGrid.gridSize.y, 3)
        XCTAssertEqual(robotDriver.planetGrid.robots.count, 3)
        XCTAssertEqual(robotDriver.robotsDestiny.count, 0)
        
        XCTAssertEqual(robotDriver.planetGrid.robots[0].position.coordinates.x, 1)
        XCTAssertEqual(robotDriver.planetGrid.robots[0].position.coordinates.y, 1)
        XCTAssertEqual(robotDriver.planetGrid.robots[0].position.direction, Direction.east)
        XCTAssertEqual(robotDriver.planetGrid.robots[0].movement,
                       [.right, .forward, .right, .forward, .right, .forward, .right, .forward])
        
        XCTAssertEqual(robotDriver.planetGrid.robots[1].position.coordinates.x, 3)
        XCTAssertEqual(robotDriver.planetGrid.robots[1].position.coordinates.y, 2)
        XCTAssertEqual(robotDriver.planetGrid.robots[1].position.direction, Direction.north)
        XCTAssertEqual(robotDriver.planetGrid.robots[1].movement,
                       [.forward, .right, .right, .forward, .left, .left, .forward, .forward, .right, .right, .forward, .left, .left])
        
        XCTAssertEqual(robotDriver.planetGrid.robots[2].position.coordinates.x, 0)
        XCTAssertEqual(robotDriver.planetGrid.robots[2].position.coordinates.y, 3)
        XCTAssertEqual(robotDriver.planetGrid.robots[2].position.direction, Direction.west)
        XCTAssertEqual(robotDriver.planetGrid.robots[2].movement,
                       [.left, .left, .forward, .forward, .forward, .left, .forward, .left, .forward, .left])
    }
    
    func testMoveRobotsToDestiny() {
        
        robotDriver = RobotDriver(planetGrid)
        robotDriver.moveRobotsToDestiny()
        
        XCTAssertEqual(robotDriver.robotsDestiny.count,
                       planetGrid.robots.count)
        
        XCTAssertEqual(robotDriver.robotsDestiny[0].status, .active)
        XCTAssertEqual(robotDriver.robotsDestiny[0].position.direction, .east)
        XCTAssertEqual(robotDriver.robotsDestiny[0].position.coordinates.x, 1)
        XCTAssertEqual(robotDriver.robotsDestiny[0].position.coordinates.y, 1)
        
        XCTAssertEqual(robotDriver.robotsDestiny[1].status, .loss)
        XCTAssertEqual(robotDriver.robotsDestiny[1].position.direction, .north)
        XCTAssertEqual(robotDriver.robotsDestiny[1].position.coordinates.x, 3)
        XCTAssertEqual(robotDriver.robotsDestiny[1].position.coordinates.y, 3)
        
        XCTAssertEqual(robotDriver.robotsDestiny[2].status, .active)
        XCTAssertEqual(robotDriver.robotsDestiny[2].position.direction, .south)
        XCTAssertEqual(robotDriver.robotsDestiny[2].position.coordinates.x, 2)
        XCTAssertEqual(robotDriver.robotsDestiny[2].position.coordinates.y, 3)
        
        XCTAssertEqual(robotDriver.robotsDestiny[0].description, "1 1 E")
        XCTAssertEqual(robotDriver.robotsDestiny[1].description, "3 3 N LOST")
        XCTAssertEqual(robotDriver.robotsDestiny[2].description, "2 3 S")
    }
    
    func testMoveRobotsToDestinyFromParsing() {
        
        let parser = Parser()
        planetGrid = parser.getPlanetGrid(Helper.loadValidFile()!)
        robotDriver = RobotDriver(planetGrid)
        robotDriver.moveRobotsToDestiny()
        
        XCTAssertEqual(robotDriver.robotsDestiny.count,
                       planetGrid.robots.count)
        
        XCTAssertEqual(robotDriver.planetGrid.gridSize.x, 5)
        XCTAssertEqual(robotDriver.planetGrid.gridSize.y, 3)
        XCTAssertEqual(robotDriver.planetGrid.robots.count, 3)
        XCTAssertEqual(robotDriver.robotsDestiny.count, 3)
        
        XCTAssertEqual(robotDriver.planetGrid.robots[0].position.coordinates.x, 1)
        XCTAssertEqual(robotDriver.planetGrid.robots[0].position.coordinates.y, 1)
        XCTAssertEqual(robotDriver.planetGrid.robots[0].position.direction, Direction.east)
        XCTAssertEqual(robotDriver.planetGrid.robots[0].movement,
                       [.right, .forward, .right, .forward, .right, .forward, .right, .forward])
        
        XCTAssertEqual(robotDriver.planetGrid.robots[1].position.coordinates.x, 3)
        XCTAssertEqual(robotDriver.planetGrid.robots[1].position.coordinates.y, 2)
        XCTAssertEqual(robotDriver.planetGrid.robots[1].position.direction, Direction.north)
        XCTAssertEqual(robotDriver.planetGrid.robots[1].movement,
                       [.forward, .right, .right, .forward, .left, .left, .forward, .forward, .right, .right, .forward, .left, .left])
        
        XCTAssertEqual(robotDriver.planetGrid.robots[2].position.coordinates.x, 0)
        XCTAssertEqual(robotDriver.planetGrid.robots[2].position.coordinates.y, 3)
        XCTAssertEqual(robotDriver.planetGrid.robots[2].position.direction, Direction.west)
        XCTAssertEqual(robotDriver.planetGrid.robots[2].movement,
                       [.left, .left, .forward, .forward, .forward, .left, .forward, .left, .forward, .left])
        
        XCTAssertEqual(robotDriver.robotsDestiny[0].status, .active)
        XCTAssertEqual(robotDriver.robotsDestiny[0].position.direction, .east)
        XCTAssertEqual(robotDriver.robotsDestiny[0].position.coordinates.x, 1)
        XCTAssertEqual(robotDriver.robotsDestiny[0].position.coordinates.y, 1)
        
        XCTAssertEqual(robotDriver.robotsDestiny[1].status, .loss)
        XCTAssertEqual(robotDriver.robotsDestiny[1].position.direction, .north)
        XCTAssertEqual(robotDriver.robotsDestiny[1].position.coordinates.x, 3)
        XCTAssertEqual(robotDriver.robotsDestiny[1].position.coordinates.y, 3)
        
        XCTAssertEqual(robotDriver.robotsDestiny[2].status, .active)
        XCTAssertEqual(robotDriver.robotsDestiny[2].position.direction, .south)
        XCTAssertEqual(robotDriver.robotsDestiny[2].position.coordinates.x, 2)
        XCTAssertEqual(robotDriver.robotsDestiny[2].position.coordinates.y, 3)
        
        XCTAssertEqual(robotDriver.robotsDestiny[0].description, "1 1 E")
        XCTAssertEqual(robotDriver.robotsDestiny[1].description, "3 3 N LOST")
        XCTAssertEqual(robotDriver.robotsDestiny[2].description, "2 3 S")
    }
    
    
}
